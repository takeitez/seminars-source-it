package com.takeiteasy.vip.fourth.model;

public class Book extends Literature {
    private String author;

    public Book(int year, String publisher, String author) {
        super(year, publisher);
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                "}\n";
    }

    @Override
    public void print() {
        System.out.println(this);
    }
}
