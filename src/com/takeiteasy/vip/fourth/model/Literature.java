package com.takeiteasy.vip.fourth.model;

public abstract class Literature {
    int year;
    String publisher;

    Literature(int year, String publisher) {
        this.year = year;
        this.publisher = publisher;
    }

    public abstract void print();
}
