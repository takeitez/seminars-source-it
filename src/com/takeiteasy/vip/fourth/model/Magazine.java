package com.takeiteasy.vip.fourth.model;

public class Magazine extends Literature {

    private int articlesCount;

    public Magazine(int year, String publisher, int articlesCount) {
        super(year, publisher);
        this.articlesCount = articlesCount;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "articlesCount='" + articlesCount + '\'' +
                ", year='" + year + '\'' +
                "}\n";
    }

    @Override
    public void print() {
        System.out.println(this);
    }
}
