package com.takeiteasy.vip.fourth;

import com.takeiteasy.vip.fourth.model.Book;
import com.takeiteasy.vip.fourth.model.Literature;
import com.takeiteasy.vip.fourth.model.Magazine;

public class Fourth {

    private Literature[] literatureArray;

    public Fourth() {
        literatureArray = createLiteratureArray();
    }

    private Literature[] createLiteratureArray() {
        Literature[] array = new Literature[5];

        array[0] = new Book(1678, "Some pub 0", "Jay Z.");
        array[1] = new Magazine(1978, "Some pub 1", 10);
        array[2] = new Book(1790, "Some pub 2", "Sam B.");
        array[3] = new Magazine(1998, "Some pub 3", 25);
        array[4] = new Book(1878, "Some pub 4", "John W.");

        return array;
    }

    public void run() {
        for (Literature literature : literatureArray)
            literature.print();
    }
}
