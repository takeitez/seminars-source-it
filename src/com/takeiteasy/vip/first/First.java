package com.takeiteasy.vip.first;

import java.util.Scanner;

public class First {
    private final Scanner scanner;

    public First(Scanner scanner) {
        this.scanner = scanner;
    }

    public void run() {
        System.out.print("Type base: ");

        int base = scanner.nextInt();

        System.out.print("Type pow: ");

        int pow = scanner.nextInt();

        System.out.println(base + "^" + pow + " = " + Math.pow(base, pow));

//        String sentence = "Some senseless sentence for play";
//        String[] words = sentence.split(DELIMITER);
//
//        printWordsUsingFor(words);
//
//        System.out.println();
//
//        printWordsUsingDoWhile(words);
//
//        System.out.println();
//
//        printWordsUsingWhile(words);
    }

    private void printWordsUsingFor(String[] words) {
        System.out.println("Using for:");

        for (String word : words)
            System.out.println(word);
    }

    private void printWordsUsingDoWhile(String[] words) {
        System.out.println("Using do while:");
        int i = 0;

        do {
            System.out.println(words[i]);
            i++;
        } while (i < words.length);
    }

    private void printWordsUsingWhile(String[] words) {
        System.out.println("Using while:");
        int i = 0;

        while (i < words.length) {
            System.out.println(words[i]);
            i++;
        }
    }
}
