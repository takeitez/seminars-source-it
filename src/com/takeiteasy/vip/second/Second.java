package com.takeiteasy.vip.second;

import java.util.Arrays;
import java.util.Scanner;

public class Second {
    private final Scanner scanner;

    public Second(Scanner scanner) {
        this.scanner = scanner;
    }

    public void run() {

//        firstPart();
       secondTask();
    }

    private void firstPart() {
        String[] strings = requestStringsInput();

        System.out.println("You've entered:");
        System.out.println(Arrays.toString(strings));

        String startsWithLatinChar = findStartWithLatinChar(strings);

        if (startsWithLatinChar != null) {
            System.out.println("String which is start with latin char:");
            System.out.println(startsWithLatinChar);
        } else {
            System.out.println("No matched string found.");
        }
    }

    private String[] requestStringsInput() {
        System.out.println("Enter number of strings to read:");

        int count = Integer.valueOf(scanner.nextLine());

        String[] strings = new String[count];

        for (int i = 0; i < count; i++) {
            System.out.println("Enter string №" + (i + 1) + ":");
            strings[i] = scanner.nextLine();
        }

        return strings;
    }

    private String findStartWithLatinChar(String[] strings) {
        for (String s : strings) {
            if (s.matches("^[a-zA-Z].*$"))
                return s;
        }

        return null;
    }

    private void secondTask() {
        System.out.println("Enter number of users to create:");
        int count = Integer.valueOf(scanner.nextLine());

        User[] users = getUsers(count);
        printUsers(users);
    }

    private User[] getUsers(int count) {
        User[] users = new User[count];

        for (int i = 0; i < count; i++) {
            User user = new User();

            System.out.println("Enter name:");
            user.name = scanner.nextLine();

            System.out.println("Enter age:");
            user.age = Integer.valueOf(scanner.nextLine());

            users[i] = user;
        }

        return users;
    }

    private void printUsers(User[] users) {
        System.out.println("You've added " + users.length + " users:");
        for (User user : users)
            System.out.println(user.name + ":" + user.age);
    }
}
